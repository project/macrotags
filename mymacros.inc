<?php

/**
 * Use this space to write your own custom macrotags. For inspiration take a look 
 * at macros.inc.
 *
 * Macros can be stored in the macrotags/mymacros.inc file or any other
 * part of Drupal's filesystem. Some module developers may want macrotag
 * functions to live in their module file instead of elsewhere.
 * 
 * A macro function is written in standard PHP and is identified by
 * prefixing the function name with 'mt_' and returning a string of
 * output.
 * 
 * function mt_mymacro($param1, $param2) {
 *   ...
 * }
 * 
 * This macro is invoked as follows:
 * 
 * [mymacro|param1|param2]
 * 
 * To provide online documentation for your macro, you need to build a
 * help function. The stub is as follows:
 * 
 * function mt_mymacro_help($section) {
 *   switch ($section) {
 *     case 'short-tip':
 *       return t('This is a quick reference');
 *       break;
 *       
 *     case 'long-tip':
 *       return t('Extended documentation goes here.');
 *       break;
 *   }
 * }
 * 
 * You can also create two-part macros. See mt_print and mt_print_hook
 * in macros.inc for implementation details.
 */
 
 

?>
