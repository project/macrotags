********************************************************************
                     D R U P A L    M O D U L E                         
********************************************************************
Name: Macrotags module
Version: 0.9
Author: Matt Westgate
Email: drupal at asitis dot org
Last update: Nov 11, 2005
Drupal: 4.6

********************************************************************
DESCRIPTION:

This module constructs a way for users to create dynamic content
using an easy syntax.

These little snippets of dynamic content are called "MacroTags". The
idea behind MacroTags is when your content is rendered, the MacroTags
are somehow "expanded" into something else.

Here is what a MacroTag looks like:

[time|now]

MacroTags are always surrounded by square brackets and the different
components inside the tag are delimited by the pipe character '|'.
When the above example MacroTag is viewed on your web site, it will
display the current time. And if you refresh your browser window, the
time will change again so that it is always current!

Likewise, you can replace the 'now' component of the macro with just
about any textual datetime description you can think of. Here are
some examples:

[time|next Thursday]
[time|1 month ago]
[time|yesterday]

When any of the above macros are viewed, they will display the
actual times based on the textual descriptions.

You can use the same macro over and over again within your content.
For example.

Today is [time|now] and 200 days ago it was
[time|200 days ago].

Or, create a hierarchical taxonomy and see it rendered nicely with

[taxonomy_tree|1]

********************************************************************
DEFAULT MACROS

There are several pre-existing macros in the macros.inc file. Here
is a description of each.

[link|URL|link text(optional)]
Description:
  Creates a hyperlink.
Example:
  [link|http://google.com|Search Google] => <a href="http://google.com">Search Google</a>


[title|title text|link text(optional)]
Description:
  Creates a hyperlink to another piece of content in your web site
  where 'title text' is the title of the content you wish to link to.
Example:
  [title|My First Story|read this first] => <a href="macrotags/title/1">read this first</a>


[time|time string|style(optional)]
Description:
  Display the time where 'time string' is a textual datetime
  description and style is 'short', 'medium', or 'long'
Example:
  [time|now|long] => Sat, 12/13/2003 - 23:57


[user_mail|name or id]
Description:
  Display an email address of an user, where 'name or id' is the
  identifier of the user whose information you wish to retrieve.
Example:
  [user_mail|mathias]
Returns:
  Them email address of user mathias


[format_size|size]
Description:
  Display the number of bytes in a human friendly format.
Example:
  [format_size|50068] => 48.89 KB


[print|nid|link text(optional)]
Description:
  Display a link for a printer friendly version of your content,
  where nid is the node id of your content.
Example:
  [print|1|printer friendly page] => <a href="macrotags/print/1">printer friendly page</a>


********************************************************************
PERMISSIONS:

This module uses role based permissions to determine who is allowed
to execute a macro when it is viewed.  Each role is assigned a list
of macros that group allowed to execute. If a user views a node
containing macros they are not allowed to execute, they will see the
entire node content and the macro in it's un-executed form.


********************************************************************
CREATING MACROS

Macros can be stored in the macrotags/mymacros.inc file or any other
part of Drupal's filesystem. Some module developers may want macrotag
functions to live in their module file instead of elsewhere.

A macro function is written in standard PHP and is identified by
prefixing the function name with 'mt_' and returning a string of
output.

function mt_mymacro($param1, $param2) {
  ...
}

This macro is invoked as follows:

[mymacro|param1|param2]

To provide online documentation for your macro, you need to build a
help function. The stub is as follows:

function mt_mymacro_help($section) {
  switch ($section) {
    case 'short-tip':
      return t('This is a quick reference');
      break;
      
    case 'long-tip':
      return t('Extended documentation goes here.');
      break;
  }
}

You can also create two-part macros. See mt_print and mt_print_hook
in macros.inc for implementation details.


********************************************************************
SYSTEM REQUIREMENTS:

Drupal: 4.6

********************************************************************
INSTALLATION:

see the INSTALL file in this directory.

********************************************************************
TODO:

- ?

********************************************************************
UPCOMING FEATURES:

- Figure out the best way to allow users to create their own macros.

