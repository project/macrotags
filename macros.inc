<?php

/* Use this space to write your own custom macrotags. */

function mt_taxonomy_tree_help($section = 'short-tip') {

  switch ($section) {
    case 'short-tip':
      $output = t('<pre>[taxonomy_tree|vid]</pre>');
      $output .= t('<p>This tag allows you take any vocabulary and display it on your site. <code>vid</code> can be the unique id of the vocabulary or you can simply pass in the vocabulary name.</p>');
      break;

    case 'long-tip':
      $output = t('<pre>[taxonomy_tree|vid]</pre>');
      $output .= t('<p>This tag allows you take any vocabulary and display it on your site. <code>vid</code> can be the unique id of the vocabulary or you can simply pass in the vocabulary name.</p>');
      $output = t('<pre>[taxonomy_tree|vid]</pre>');
      $output .= t('<p>This tag allows you take any vocabulary and display it on your site. <code>vid</code> can be the unique id of the vocabulary or you can simply pass in the vocabulary name.</p>');
      $output .= t('<p>There are many parameters you can send along with this tag. Here\'s the full list:</p>');
      $output .= t('<pre>[taxonomy_tree|
 vid| (vocabulary id or name)
 order_by| (SQL ORDER_BY clause. Leave blank if this doesn\'t make sense.)
 dsp_nodes| (True or False. Should the nodes be displayed?)
 dsp_empties| (True or False. Should empty taxonomy terms be displayed?)
 dsp_vocab_names| (True or False. Should the vocabulary name be displayed?)
 dsp_count| (True or False. Should the number of nodes per term be displayed?)
 node_type (Name of nodetype to exclude.)
]</pre>');
    break;
  }

  return $output;
}

/**
 * Display a taxonomy in a myriad of ways.
 *
 * @param $vid
 *   The vocabulary name or id to express.
 *
 * @param $order_by
 *   The order you want the database to return the resultset. For example, if
 *   you want to sort by last modification time, you would pass in:
 *   'changed DESC'
 *
 * @param $dsp_nodes
 *   Display node associated with the terms.
 *
 * @param $dsp_empties
 *   Display terms with no nodes.
 *
 * @param $dsp_vocab_name
 *   Display the vocabulary name.
 *
 * @param $dsp_count
 *   Display the number of nodes found for a term.
 *
 * @param $node_type
 *   Filter node display to a certain node type.
 *
 */
function mt_taxonomy_tree($vid, $order_by = '', $dsp_nodes = true,
                          $dsp_empties = false, $dsp_vocab_name = false, $dsp_count = false, $node_type = 0) {

  if ($order_by == "") {
    $order_by = 'n.title DESC, n.created DESC';
  }

  $v = (is_numeric($vid)) ? taxonomy_get_vocabulary($vid) : taxonomy_get_vocabulary_by_name($vid);
  $v = (is_array($v)) ? $v[0] : $v;
  $tree_nodes = unserialize(cache_get("taxonomy_dhtml:tree_nodes_$node_type". $v->vid));

  if (!$tree_nodes) {
    $tree = taxonomy_get_tree($v->vid);
    foreach ($tree as $key => $term) {

      $count = taxonomy_term_count_nodes($term->tid, $node_type);
      if ($count > 0 || $dsp_empties) {
        $term->name = ucwords(strtolower(t($term->name)));
        $term->name .= ($dsp_count) ? " ($count)" : '';
        $tree[$key] = $term;
      }
      else {
        unset($tree[$key]);
      }
    } // End for loop

    $tree_nodes = ($dsp_nodes) ? macrotags_taxonomy_inject_nodes($tree, $node_type, $order_by) : $tree;
    cache_set("taxonomy_dhtml:tree_nodes_$node_type". $v->vid, serialize($tree_nodes), time() + variable_get("cache_clear", 120));
  }

  $boxes[$n]["content"] = theme('macrotag_taxonomy_outline', $tree_nodes);
  $boxes[$n]["subject"] = $v->name;
  $boxes = ($boxes) ? $boxes : array();

  $output = "";
  foreach ($boxes as $box) {
    $output .= "<div class=\"mt_vocabulary\">\n";
    if ($dsp_vocab_name) {
      $output .= "<h3>". ucwords(strtolower($box['subject'])). "</h3>\n";
    }
    $output .= $box['content']. "\n";
    $output .= "</div>\n";
  }

  return $output;
}

// given a taxonomy tree, add nodes below all relevant terms
function macrotags_taxonomy_inject_nodes($tree, $type = NULL, $order_by = 'n.title DESC, n.created DESC') {
  
  // Rebuild the array index. The keys need to start at zero. Is this the fastest way?
  foreach ($tree as $term) {
    $tree_node[] = $term;  
  }
  $tree = $tree_node;
  
  // iterate over the tree backwards, so I don't trip on the new items.
  // I haven't analyzed this function, but if it didn't care about zero-based 
  // keys, i could drop the above for loop!
  for ($i=count($tree)-1; $i>=0 ; $i--) {
    $term = $tree[$i];
    /* restrict to a single type if given */
    $type_q = ($type ? " AND n.type = '$type'" : "");
    $result = db_query_range("SELECT n.nid, n.title, n.teaser, n.type, u.uid, u.name FROM {term_node} r LEFT JOIN {node} n ON r.nid = n.nid LEFT JOIN {users} u ON n.uid = u.uid WHERE n.status = '1' $type_q AND r.tid = '$term->tid' ORDER BY $order_by", 0, 50);
    while ($node = db_fetch_object($result)) {
      $img = '';
      if (module_exist('comment')) {
        $detail = t("Author: %name, comments: %num", array ("%name" => strip_tags(format_name($node)), "%num" => comment_num_all($node->nid)));
      }
      
      if ($node->type == 'filestore2') {
        $fs = filestore2_load($node);
        
        $img = (strstr($fs->mimetype, 'pdf')) ? '<img style="display:inline;" src="images/pdf.png" hspace="5" alt="PDF icon" />' : "";
        $img = l($img, "filestore2/download/$node->nid", array("title" => t("download $fs->filename")), NULL, NULL, FALSE, true). '<span style="color:#666;font-size:10px;">('. format_size($fs->size) .')</span>';
      }
      elseif ($node->type == 'link') {
        $l = link_load($node);
        
        if (strstr($l->url, 'pdf')) {
          $img = '<img style="display:inline;" src="images/pdf.png" hspace="5" alt="PDF icon" />';
        }
        else {
          $img = '<img style="display:inline;" src="images/www.png" hspace="5" alt="link icon" />';
        }
        
        $img = l($img, "$l->url", array("title" => t("Visit $l->url")), NULL, NULL, FALSE, true);
      }
      
      $link = l($node->title, "node/$node->nid", array ("title" => $detail)). ' '. $img;
      
      $term_node = array2object(array ("nid" => $node->nid, "depth" => $term->depth+1, "link" => $link));
      $part1 = array_slice($tree_node, 0, $i+1);
      $part2 = array_slice($tree_node, $i+1, count($tree_node));
      $part1[] = $term_node;
      $tree_node = array_merge($part1, $part2);
    }
  }
  return $tree_node;
}

function theme_macrotag_taxonomy_outline($tree) {
    
  $old_depth = -1;
  $curr = 0;
  $output = "";
  
  foreach ($tree as $key => $term) {
    
    if ($term->depth > $old_depth) {
      $output .= "<ul class=\"".($term->depth == 0 ? 'mt_menulist' : 'mt_submenu'). "\">\n";
    }
    if ($term->depth < $old_depth) {
      $delta = $old_depth - $term->depth;
      $output .= str_repeat("</ul>\n</li>", ($delta > 0 ? $delta : 0 ));
    }
    
    $old_depth = $term->depth;      
    // if children exist, output with proper class and id attributes, else, output item with specified link or default link
    if ($term->depth < $tree[$curr+1]->depth) {
      $link = l(t($term->name), "taxonomy/term/". $term->tid, array("title" => t($term->description)));
      $output .= '<li class="mt_menubar"><div class="mt_menubar_anchor">'.$link."</div>\n";
    }
    else if ($term->link) {
      $link = $term->link;
      $output .= "<li>$link</li>\n";  
    } 
    else {
      $link = l(t($term->name), "taxonomy/term/". $term->tid, array("title" => t($term->description)));
      $output .= "<li>$link</li>\n";  
    }
    
    $curr ++;
  }// End for loop
  $output .= str_repeat("</ul>\n</li>\n", $term->depth). "</ul>\n";
  
  return $output;
}

function mt_taxonomy_term_help($section = 'short-tip') {

  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[taxonomy_term|tid]</pre>');
      $output .= t("<p>This tag allows you to take any taxonomy term and display a list of nodes that are associated with it. The <code>tid</code> parameter can either be the unique id of the term or the term name.</p>");
      break;
  }

  return $output;
}

function mt_taxonomy_term($tid, $order_by = '') {

  foreach (explode(',', $tid) as $t) {
    $t = trim($t);
    if (!is_numeric($t)) {
      $term = taxonomy_get_term_by_name($t);
      $tids_str .= $term[0]->tid. ',';
    }
    else {
      $tids_str .= $t. ',';
    }
  }
  $tids_str = substr($tids_str, 0, -1);

  $taxonomy->operator = "or";
  $taxonomy->tids = $tids_str;
  $taxonomy->str_tids = $tids_str;

  if ($order_by == "") {
    $order_by = 'static DESC, n.title DESC, n.created DESC';
  }

  global $user;

  if ($taxonomy->str_tids) {
    if ($taxonomy->operator == "or") {
      $sql = "SELECT DISTINCT(n.nid), n.title, n.type, n.created, n.changed, n.uid, n.static, n.created, u.name FROM {node} n INNER JOIN {term_node} r ON n.nid = r.nid INNER JOIN {users} u ON n.uid = u.uid WHERE r.tid IN ($taxonomy->str_tids) AND n.status = '1' ORDER BY $order_by";
      $sql_count = "SELECT COUNT(DISTINCT(n.nid)) FROM {node} n INNER JOIN {term_node} r ON n.nid = r.nid INNER JOIN {users} u ON n.uid = u.uid WHERE r.tid IN ($taxonomy->str_tids) AND n.status = '1'";
    }
    else {
      $sql = "SELECT n.nid, n.title, n.type, n.created, n.changed, n.uid, u.name FROM {node} n INNER JOIN {term_node} r ON n.nid = r.nid INNER JOIN {users} u ON n.uid = u.uid WHERE r.tid IN ($taxonomy->str_tids) AND n.status = '1' GROUP BY n.nid, n.title, n.type, n.created, n.changed, n.uid, u.name HAVING COUNT(n.nid) = ". count($taxonomy->tids) .$order_by;

      // Special trick as we could not find anything better:
      $count = db_num_rows(db_query("SELECT n.nid FROM {node} n INNER JOIN {term_node} r ON n.nid = r.nid WHERE r.tid IN ($taxonomy->str_tids) AND n.status = '1' GROUP BY n.nid HAVING COUNT(n.nid) = ". count($taxonomy->tids)));
      $sql_count = "SELECT $count";
    }

    if ($pager) {
      $result = pager_query($sql, variable_get("default_nodes_main", 10) , 0, $sql_count);
    }
    else {
      $result = db_query_range($sql, 0, 15);
    }
  }

  return node_title_list($result, "");
}

function mt_link_help($section = 'short-tip') {

  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[link|URL|link text(optional)]</pre>');
      $output .= t("<p>This tag allows you to create a hyperlink, where 'URL' is the address you wish to link to and optional 'link text' is the words you want to be linked.</p>");
      break;
  }

  return $output;
}

/**
 * Create hyperlinks
 */
function mt_link($url, $text = null) {
  if (!$text) {
    $text = $url;
  }

  if (valid_email_address($url)) {
    $url = "mailto:$url";
  }

  return (strstr($url, 'http://') || strstr($url, 'mailto:')) ? "<a href=\"$url\">$text</a>" : l($text, $url);
}

function mt_title_help($section = 'short-tip') {
  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[title|title text|link text(optional)]</pre>');
      $output .= t("<p>Creates a hyperlink to another piece of content in your web site where 'title text' is the title of the content you wish to link to.</p>");
      break;
  }
  return $output;
}

/**
 * Linking to another node via title
 */
function mt_title($title, $linktext = NULL) {
  /*
  if (!module_exist('title')) {
    $nid = db_result(db_query("SELECT nid FROM {node} WHERE title = '%s'", $title));
    $uri = "node/view/$nid";
  }
  else {
    $uri = "macrotags/title/".urlencode($title);
  }
  */
  $uri = "macrotags/title/".urlencode($title);

  if (!$linktext) {
    $linktext = $title;
  }

  return l($linktext, $uri);
}

function mt_title_page_hook($title) {
  if (user_access("access content")) {

    $title = urldecode($title);

    /* The title is an external link. */
    if (strstr($title, 'http://')) {
      drupal_goto($title);
    }

    $result = db_query("SELECT n.*, u.name, u.uid FROM {node} n INNER JOIN {users} u ON n.uid = u.uid WHERE n.title = '%s' AND n.status = 1 ORDER BY n.created DESC", $title);

    if (db_num_rows($result) == 0) {
      // No node with exact title found, try substring.
      $result = db_query("SELECT n.*, u.name, u.uid FROM {node} n INNER JOIN {users} u ON n.uid = u.uid WHERE n.title LIKE '%". check_query($title) ."%' AND n.status = 1 ORDER BY n.created DESC");
    }

    if (db_num_rows($result) == 0 && module_exist("search")) {
      // still no matches ... return a full text search
      search_view($title);
    }
    else if (db_num_rows($result) == 1) {
      $node = db_fetch_object($result);
      $node = node_load(array("nid" => $node->nid));
      return theme("page", node_view($node, 0, 1), $node->title);
    }
    else {
      $header = array(t("Type"), t("Title"), t("Author"));
      while ($node = db_fetch_object($result)) {
        $type = ucfirst(module_invoke($node->type, "node", "name"));
        $title = l($node->title, "node/view/$node->nid");
        $author = format_name($node);
        $rows[] = array(array("data" => $type, "class" => "type"), array("data" => $title, "class" => "content"), array("data" => $author, "class" => "author"));
      }

      $output  = "<div id=\"title\">";
      $output .= ($rows) ? theme("table", $header, $rows) : t('<p>No matches found.</p>');
      $output .= "</div>";

      drupal_set_title(t("Matching Posts"));
      return theme("page", $output);
    }
  }
  else {
    return theme("page", message_access());
  }
}

function mt_time_help($section = 'short-tip') {
  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[time|time string|style(optional)]</pre>');
      $output .= t("<p>Display the time where 'time string' is a textual datetime description and style is 'short', 'medium', or 'long'</p>");
      break;
  }
  return $output;
}

/**
 * Display the time
 */
function mt_time($time, $type = "medium", $format = "") {
  return format_date(strtotime($time), $type, $format);
}

function mt_user_mail_help($section = 'short-tip') {
  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[user_mail|name or id]</pre>');
      $output .= t("<p>Display an email address of an user, where 'name or id' is the identifier of the user whose information you wish to retrieve.</p>");
      break;
  }
  return $output;
}

/**
 * Grab an users email address.
 */
function mt_user_mail($name_or_id) {
  if (is_int((int)$name_or_id)) {
    $email = db_result(db_query("SELECT mail FROM {users} WHERE uid = '%d'", $name_or_id));
  }
  else {
    $email = db_result(db_query("SELECT mail FROM {users} WHERE name = '%s'", $name_or_id));
  }

  return $email;
}

function mt_format_size_help($section = 'short-tip') {
  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[format_size|size]</pre>');
      $output .= t("<p>Display the number of bytes in a human friendly format.</p>");
      break;
  }
  return $output;
}

/**
 * Convert bytes to a commen sense format
 */
function mt_format_size($size) {
  return format_size($size);
}

function mt_print_help($section = 'short-tip') {
  switch ($section) {
    case 'short-tip':
    case 'long-tip':
      $output = t('<pre>[print|nid|link text(optional)]</pre>');
      $output .= t("<p>Display a link for a printer friendly version of your content, where nid is the node id of your content.</p>");
      break;
  }
  return $output;
}

/**
 * Create printer friendly pages
 */
function mt_print($nid, $text = 'Printer friendly page') {
  $path = request_uri();
  if (strstr($path, 'macrotags/print')) {
    return NULL;
  }
  else {
    return l($text, "macrotags/print/$nid");
  }
}

function mt_print_page_hook($nid) {
  if (user_access("access content")) {

    global $base_url;
    /* We can take a node id or a node title */
    if (is_numeric($nid)) {
      $result = db_query("SELECT n.nid FROM {node} n WHERE n.status = 1 AND n.nid = %d AND (n.moderate = 0 OR n.revisions IS NOT NULL) ORDER BY n.title", $nid);
    } else {
      $result = db_query("SELECT n.*, u.name, u.uid FROM {node} n INNER JOIN {users} u ON n.uid = u.uid WHERE n.title = '%s' AND n.status = 1 ORDER BY n.created DESC", $nid);
    }

    while ($page = db_fetch_object($result)) {
      // load the node:
      $node = node_load(array("nid" => $page->nid));

      if ($node) {
        // output the content:
        if (module_hook($node->type, "content")) {
          $node = node_invoke($node, "content");
        }
        $output .= "<h1 id=\"$node->nid\" name=\"$node->nid\" class=\"print-h$depth\">$node->title</h1>";

        if ($node->body) {
          $output .= $node->body;
        }
      }
    }

    $html = "<html>\n<head>\n<title>$node->title</title>\n";
    $html .= "<base href=\"$base_url/\" />\n";
    $html .= "<style type=\"text/css\">\n@import url(misc/print.css);\n</style>\n";
    $html .= "</head>\n<body>\n". $output ."</body>\n</html>";

    return $html;
  }
  else {
    return theme("page", message_access());
  }
}